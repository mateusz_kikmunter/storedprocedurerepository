﻿using System.Linq;
using StoredProcedureRepository.Infrastructure.Extensions;
using StoreProcedureRepository.Services;

namespace StoredProcedureRepository.Infrastructure.Services
{
    public static class StoredProcedureNameFormatter
    {
        public static string GetStoredProcedureNameWithParameters(string spName, object param)
        {
            Guard.ThrowIfStringNullOrWhiteSpace(spName);
            if (param == null)
            {
                return spName;
            }

            var names = param.GetType().GetProperties().Select(p => p.Name).ToList();
            names.ForEach(name => spName += $" @{name},");

            return spName.RemoveLastCharacter();
        }
    }
}
