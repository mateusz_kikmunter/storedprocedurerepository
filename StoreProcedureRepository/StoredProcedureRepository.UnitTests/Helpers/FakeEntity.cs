﻿namespace StoredProcedureRepository.UnitTests.Helpers
{
    internal class FakeEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
